#!/usr/bin/perl

use strict;
use warnings;
use utf8;

use Getopt::Long qw(GetOptionsFromArray);
use Pod::Usage;
use DDP;

Getopt::Long::Configure (qw{
    posix_default bundling no_auto_abbrev no_ignore_case_always});
binmode(STDIN, ':utf8');
binmode(STDOUT, ':utf8');
binmode(STDERR, ':utf8');

my %char_to_num;
my %num_to_char;
my $path_to_text = './text/';

sub parse_args {
    my $args_ref = shift;
    my $config_ref = shift;

    my $help;
    my $man;

    GetOptionsFromArray($args_ref, $config_ref,
        'file=s',
        'key=s',
        'help|v|?' => \$help,
        'man' => \$man
    ) or pod2usage(2);
    pod2usage(1) if $help;
    pod2usage(-exitval => 0, -verbose => 2) if $man;
}

sub read_file {
    my $filename = shift;
    my $content_ref = shift;
    open(my $fh, '<:utf8', $filename) or die "cannot open file $filename";
    {
        local $/;
        $$content_ref = <$fh>;
        $$content_ref =~ s/\n(?=.+)/ /g;
    }
    close($fh);
}

sub create_hash {
    my $char_hash_ref = shift;
    my $num_hash_ref = shift;
    my $char_begin = unpack 'U', 'А';
    my $char_end = unpack 'U', 'Я';

    for (my $char = $char_begin; $char <= $char_end; $char++) {
        if ($char < unpack 'U', 'Е') {
            $char_hash_ref->{chr($char)} = $char - $char_begin;
            $num_hash_ref->{$char - $char_begin} = chr($char);
        } elsif ($char == unpack 'U', 'Е') {
            $char_hash_ref->{chr($char)} = $char - $char_begin;
            $char_hash_ref->{'Ё'} = $char - $char_begin + 1;
            $num_hash_ref->{$char - $char_begin} = chr($char);
            $num_hash_ref->{$char - $char_begin + 1} = 'Ё';
        } elsif ($char > unpack 'U', 'Е')  {
            $char_hash_ref->{chr($char)} = $char - $char_begin + 1;
            $num_hash_ref->{$char - $char_begin + 1} = chr($char);
        }
    }
}

my %config = (
    file    => $path_to_text . 'text.txt',
    key     => 'ТЕСТ'
);
my $content;
my $result_content;

parse_args(\@ARGV, \%config);
read_file($config{file}, \$content);
create_hash(\%char_to_num, \%num_to_char);

$config{key} = <>;
chomp $config{key};

my @content_as_array = split //, $content;
my @result_content_as_array;
my @key_as_array = split //, $config{key};
my $ind_help = -1;

for (my $i = 0; $i < length $content; $i++) {
    my $char = $char_to_num{$content_as_array[$i]};
    if (not defined $char) {
        $result_content_as_array[$i] = " ";
        next;
    }
    $ind_help++;
    $char -= $char_to_num{$key_as_array[$ind_help % length $config{key}]};
    $char += 33 if $char < 0;
    $result_content_as_array[$i] = $num_to_char{$char};
}

$result_content = join '', @result_content_as_array;
print "Исходный текст: \n";
p $content;
print "Конечный текст: \n";
p $result_content;

__END__

=head1 NAME

visioner - decode text encoded via Vigenère cipher

=head1 SYNOPSIS

./countbigrams.pl [options]

=head1 OPTIONS

=over 8

=item B<--file>

Relative or absolute path to text file.

=item B<--type>

Type of ngrams to process.

Default: bigrams.

Allowed options: bi, tri, quadro.

=item B<--help>

Print help message and exits.

=item B<--man>

Prints the manual page and exits.

=back

=head1 DESCRIPTION

B<This program> will read the given input file(s) and do something

useful with the contents thereof.

=cut
