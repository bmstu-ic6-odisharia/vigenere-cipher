#!/usr/bin/perl

use strict;
use warnings;
use utf8;

use Getopt::Long qw(GetOptionsFromArray);
use Pod::Usage;

Getopt::Long::Configure (qw{
    posix_default bundling no_auto_abbrev no_ignore_case_always});
binmode(STDOUT, ':utf8');
binmode(STDERR, ':utf8');

my %num_from_str = (
    'bi'        => 2,
    'tri'       => 3,
    'quadro'    => 4
);
my $path_to_text = './text/';

sub numerically { # compare two words numerically
    my %opts = shift;
    my %count = shift;
    if ($opts{method} eq "decrease") {
        $count{$b} <=> $count{$a}; # decreasing order
    } elsif ($opts{method} eq "increase") {
        $count{$a} <=> $count{$b}; # increasing order
    }
}

sub parse_args {
    my $args_ref = shift;
    my $config_ref = shift;

    my $help;
    my $man;

    GetOptionsFromArray($args_ref, $config_ref,
        'file=s',
        'type=s',
        'save-single-ngrams!' => \$config_ref->{save_single_ngrams},
        'help|h|?' => \$help,
        'man' => \$man
    ) or pod2usage(2);
    pod2usage(1) if $help;
    pod2usage(-exitval => 0, -verbose => 2) if $man;
}

sub read_file {
    my $filename = shift;
    my $content_ref = shift;
    open(my $fh, '<:utf8', $filename) or die "cannot open file $filename";
    {
        local $/;
        $$content_ref = <$fh>;
        $$content_ref =~ s/\n(?=.+)/ /g;
    }
    close($fh);
}

sub find_ngrams {
    my $content = shift;
    my $config = shift;

    my $num = $num_from_str{$config->{type}};
    my $save_single = $config->{save_single_ngrams};
    my %ngrams;
    $content =~ s/[\s\W]*//ug;

    for (my $i = 0; $i < length($content) - $num + 1; $i++) {
        my $ngram = substr $content, $i, $num;
        if (not defined $ngrams{$ngram}{count}) {
            $ngrams{$ngram}{count} = 1;
        } else {
            $ngrams{$ngram}{count}++;
        }
    }

    if (not $save_single) {
        delete @ngrams {
            grep {
                $ngrams{$_}{count} == 1;
            } keys %ngrams
        }
    }

    foreach (keys %ngrams) {
        my $ngram = $_;
        my @pos_array;
        push @pos_array, pos $content while $content =~ /$ngram/g;
        for (my $i = 0; $i < scalar @pos_array - 1; $i++) {
            push @{$ngrams{$ngram}{delta}}, $pos_array[$i + 1] - $pos_array[$i];
        }
    }

    return \%ngrams;
}

my %config = (
    file                => $path_to_text . 'text.txt',
    type                => 'bi',
    save_single_ngrams  => ''
);
my $content;

parse_args(\@ARGV, \%config);
read_file($config{file}, \$content);
my $ngrams = find_ngrams($content, \%config);

print $content;
print $config{type} . "gram\tcount\tdeltas\n";
foreach (keys %$ngrams) {
    my $key = $_;
    print "$key\t" . $ngrams->{$key}{count} . "\t";
    print join(",", @{$ngrams->{$key}{delta}}) . "\n";
}

 __END__

=head1 NAME

countbigrams - searching ngrams, count them and compute deltas

=head1 SYNOPSIS

./countbigrams.pl [options]

=head1 OPTIONS

=over 8

=item B<--file>

Relative or absolute path to text file.

=item B<--type>

Type of ngrams to process.

Default: bigrams.

Allowed options: bi, tri, quadro.

=item B<--help>

Print help message and exits.

=item B<--man>

Prints the manual page and exits.

=back

=head1 DESCRIPTION

B<This program> will read the given input file(s) and do something

useful with the contents thereof.

=cut
