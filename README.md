# Vigenère cipher
Some utilities, helping to decode text encoded via Vigenère cipher.

## countbigrams.pl
countbigrams — searching ngrams, count them and compute deltas.

## visioner.pl
visioner — decode text encoded via Vigenère cipher.
